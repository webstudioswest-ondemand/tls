<?php
/**
 * Functions
 *
 * @package WP Pro Real Estate 7
 * @subpackage Admin
 */

// Define some constant paths
define('ADMIN_PATH', get_template_directory() . '/admin/');
define('INC_PATH', get_template_directory() . '/includes/');

/*-----------------------------------------------------------------------------------*/
/* Load ReduxFramework */
/*-----------------------------------------------------------------------------------*/

require_once( ADMIN_PATH . 'ReduxFramework/redux-extensions-loader/loader.php' );
require_once( ADMIN_PATH . 'ReduxFramework/ReduxCore/framework.php' );
require_once( ADMIN_PATH . 'ReduxFramework/theme-options/re7-config.php' );

/*-----------------------------------------------------------------------------------*/
/* Register Sidebars */
/*-----------------------------------------------------------------------------------*/

if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name' => 'Listing Single Right',
		'id' => 'listings-single-right',
		'description' => 'Widgets in this area will be shown in the right sidebar area on the listings single view.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Left Sidebar Pages',
        'id' => 'left-sidebar-pages',
        'description' => 'Widgets in this area will be shown in the left sidebar area of pages with the left sidebar page template.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name' => 'Right Sidebar Pages',
		'id' => 'right-sidebar-pages',
		'description' => 'Widgets in this area will be shown in the right sidebar area of pages.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));
 
if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name' => 'Right Sidebar Blog',
		'id' => 'right-sidebar-blog',
		'description' => 'Widgets in this area will be shown in the right sidebar area of archives.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));
 
if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name' => 'Right Sidebar Single',
		'id' => 'right-sidebar-single',
		'description' => 'Widgets in this area will be shown in the right sidebar area of single posts.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Right Sidebar Contact Page',
        'id' => 'right-sidebar-contact',
        'description' => 'Widgets in this area will be shown in the right sidebar area of the contact page template.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s left">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'dsIDXpress Homepage',
        'id' => 'dsidxpress-homepage',
        'description' => 'This is meant to only be used with the dsIDXpress Search Widget, to replace the advanced search on the homepage. If the block has been enabled via WP Pro Real Estate 7 Options > Homepage > Layout Manager > dsIDXpress Search.',
        'before_widget' => '<aside id="%1$s" class="widget col span_3 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Four Column Homepage',
        'id' => 'four-column-homepage',
        'description' => 'Widgets in this area will be shown in the homepage if the block has been enabled via WP Pro Real Estate 7 Options > Homepage > Layout Manager > Four Column Widgets.',
        'before_widget' => '<aside id="%1$s" class="widget col span_3 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name' => 'Footer',
		'id' => 'footer',
		'description' => 'Widgets in this area will be shown in the footer.',
        'before_widget' => '<aside id="%1$s" class="widget col span_3 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h5>',
        'after_title' => '</h5>',
));

// Add Automatic Feed Links
add_theme_support( 'automatic-feed-links' );

// Localization Support
load_theme_textdomain( 'contempo', get_template_directory() . '/languages' );
 
$locale = get_locale();
$locale_file = get_template_directory() . "/languages/$locale.php";
if ( is_readable($locale_file) )
    require_once($locale_file);

/*-----------------------------------------------------------------------------------*/
/* Framework Functions
/*-----------------------------------------------------------------------------------*/

function ct_is_plugin_active( $plugin ) {
    return in_array( $plugin, (array) get_option( 'active_plugins', array() ) );
}

// Child Theme Creator
require_once (ADMIN_PATH . 'ct-child-theme/ct-child-theme.php');

// Custom Profile Fields
require_once (ADMIN_PATH . 'profile-fields.php');

// Plugin Activation
require_once (ADMIN_PATH . 'plugins/class-tgm-plugin-activation.php');
require_once (ADMIN_PATH . 'plugins/register.php');

// Aqua Resizer
require_once (ADMIN_PATH . 'aq-resizer/aq_resizer.php');

// Theme Functions
require_once (ADMIN_PATH . 'theme-functions.php');

// Theme Hooks
require_once (ADMIN_PATH . 'theme-hooks.php');

// CT Social Widget
require_once (ADMIN_PATH . 'ct-social/ct-social.php');

// Widgets
require_once (INC_PATH . 'widgets.php');

add_action('wp_head', 'ct_wp_head');

?>